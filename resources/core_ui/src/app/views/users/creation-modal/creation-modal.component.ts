import { Component, Input, Output, ViewChild, EventEmitter, OnInit } from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';

// services
import swal from 'sweetalert2';
import { ClientService as CRUDService } from '../../../services/client.service';

import { showPreconfirmMessage } from '../../../shared/helpers';
import { Observable } from 'rxjs';

@Component({
  selector: 'creation-modal',
  templateUrl: './creation-modal.component.html',
  styleUrls: ['./creation-modal.component.css'],
  providers: [
  ]
})
export class CreationModalComponent implements OnInit {

  @ViewChild('modalCR')
  public modal: ModalDirective;
  
  @ViewChild('formCR')
  public form:NgForm;
  
  @Output()
  public reloadTable: EventEmitter<any> = new EventEmitter();

  public loadingBatches:boolean;
  
  public requestData: any;
  public sendingForm:boolean;


  public client_form:FormGroup;



  constructor(
    private _crudService: CRUDService,
    private fb: FormBuilder
   )
  {


     this.client_form = this.fb.group({
      _method:new FormControl('POST'),
      client_id:new FormControl(null),
      first_name: new FormControl('',Validators.required),
      last_name: new FormControl('',Validators.required),
      identification_card: new FormControl('',Validators.required),
      direction: new FormControl('',Validators.required),
      local_telephone: new FormControl('',Validators.required),
      phone: new FormControl('',Validators.required),
      emails: this.fb.array([
      this.fb.control('')
        ])

    });



    this.requestData = {
      name: null,
      identifier: null
    };

    this.sendingForm = false;

    this.loadingBatches = false;
  }

  ngOnInit():void
  {
  }

  get emails() {
  return this.client_form.get('emails') as FormArray;
  }

 get client_form_control() {
    return this.client_form.controls;
  }



  get is_invalid() {
    return this.client_form.invalid;
  }
  
  
  public addEmails() {
  this.emails.push(this.fb.control(''));

  }

  public removeEmails() {
  const value = this.emails.value;

  this.emails.removeAt(value.length - 1)
  }


  public async showPreconfirmMessage():Promise<void>
  {

  let message:string='';

  switch(this.client_form_control._method.value) { 
   case 'POST': { 

      message='Añadir Cliente?';

      break; 
   } 
   case 'PUT': { 
      message='Modificar Cliente?';

      break; 
   } 
  
   } 


  const response = await showPreconfirmMessage(
                            message,
                            "",
                            "info",
                            "Cancelar",
                            "Si"
                            );

    if (response.value)
      this.sendForm();
  }

  private sendForm():void
  {
  	if (this.sendingForm)
  		return;

    this.sendingForm = true;

    console.log(this.client_form_control._method.value);

   switch(this.client_form_control._method.value) { 
   case 'POST': { 

         this._crudService.store(this.client_form.value).subscribe(
      response => this.handleResponse(response),
      err =>{
        console.error('Observer got an error: ' + err)
            this.sendingForm = false

    },
      () => {
    this.sendingForm = false

      }
     );

      break; 
   } 
   case 'PUT': { 
         this._crudService.update(this.client_form.value).subscribe(
      response => this.handleResponse(response),
      err =>{
        console.error('Observer got an error: ' + err)
            this.sendingForm = false

    },
      () => {
    this.sendingForm = false

      }
     );
      break; 
   } 
  
   } 



   



  }

  private handleResponse(response):void
  {
    const action = () => {
      this.reloadTable.emit(true);
      this.modal.hide();
    };

    swal.fire('Exito!',response.message,'success')
        .then( () =>  action())
        .catch( () => action());
  }

  public clearForm():void
  {
    this.client_form.reset();
    this.emails.clear();
    this.addEmails();
    this.sendingForm = false;
   


  }

}
