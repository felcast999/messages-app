import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { formatDate } from '../../../shared/helpers';

@Component({
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.scss']
})
export class DetailsViewComponent implements OnInit {

	public client;
  public emails;

  constructor(
  	private route:ActivatedRoute
  ) { }

  ngOnInit()
  {
  	this.route.data.subscribe((data:any) =>{ 
      this.client = data.model.client
      this.emails = data.model.emails

    });
  }

  public formatDate(date:string|Date, format?:string):string
  {
  		return formatDate(date,format);
  }
}  
