import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';
import { formatDate, now, showPreconfirmMessage } from '../../shared/helpers';
import { ClientService as CRUDService } from '../../services/client.service';
import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';


@Component({
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements AfterViewInit {



  @ViewChild(CreationModalComponent)
  public creattionModal: CreationModalComponent;



  @ViewChild(TableComponent)
  public table: TableComponent;

  constructor(
    private _crudService:CRUDService,
    private toast: ToastrService,
    private fb: FormBuilder
  //  private spinner: NgxSpinnerService
  ) { }

  ngAfterViewInit(): void {
/*
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 5
*/

    this.table.init();
  }

  public reloadTable(): void {
    this.table.reload();
  }

  public showCreationModal(event:any):void{


    if(typeof event !== 'undefined')
    {

      //getting user
      this._crudService.show(event.id).subscribe(data=>{


         this.creattionModal.client_form.patchValue({
         _method: 'PUT',
          client_id:event.id,
          first_name: data.client.first_name,
          last_name: data.client.last_name,
          identification_card: data.client.identification_card,
          direction: data.client.direction,
          local_telephone: data.client.local_telephone,
          phone: data.client.phone,
        });

          this.creattionModal.emails.clear()
          data.emails.forEach(b => {
         this.creattionModal.emails.push(this.fb.control(b));
      });

         this.creattionModal.modal.show();

      })


    }else{
    this.creattionModal.client_form.controls['_method'].setValue('POST');
    this.creattionModal.client_form.controls['client_id'].setValue(null);  
    
    this.creattionModal.modal.show();

    }

   

  
  }



    public delete(event:any):void
  {
    showPreconfirmMessage(
      "¿Eliminar cliente?",
      ""
    )
    .then(result => {

      if (result.value)
      {
        this._crudService.delete(event.id).subscribe(
          response => this.handleResponseOfRequest(response));
      }

    });
  }




  private handleResponseOfRequest(response:any):void
  {
    this.toast.success(response.message, 'Exito!');
    this.reloadTable();
  }

}
