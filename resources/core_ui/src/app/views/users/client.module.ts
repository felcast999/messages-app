import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ClientRoutingModule } from './client-routing.module';

import { ClientComponent } from './client.component';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';
import { DetailsViewComponent } from './details-view/details-view.component';

import { ClientService } from '../../services/client.service';
import {  AuthInterceptor } from '../../services/http-interceptors/auth-interceptor.service';


import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { DataTablesModule } from 'angular-datatables';
import { HttpClient, HttpHeaders, HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  declarations: [
    ClientComponent,
    CreationModalComponent,
    TableComponent,
    DetailsViewComponent,
    ],
  imports: [
    ClientRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,

  ],
  providers: [
    ClientService,
    { 
   provide: HTTP_INTERCEPTORS,
   useClass: AuthInterceptor, 
   multi: true 
    }
  ]
})
export class ClientModule { }
