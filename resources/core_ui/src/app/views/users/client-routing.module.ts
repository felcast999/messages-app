import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientComponent } from './client.component';
import { DetailsViewComponent } from './details-view/details-view.component';

// resolvers
import { ShowResolverService } from '../../services/solvers/client/show-resolver.service';


const routes: Routes = [{
  path: '',
  data: {
    title: 'Clientes'
  },
  children: [
    
    {
      path: '',
      component: ClientComponent,
      data: {
        title: "Clientes"
      }
    },
    {
      path: ':id',
      component: DetailsViewComponent,
      data: {
        title: "Detalles de cliente"
      },
      resolve: {
        model: ShowResolverService
      }
    }
  ] 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ShowResolverService
  ]
})
export class ClientRoutingModule { }
