import { Component, HostBinding, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { JwtService } from '../../services/auth/jwt.service';
import { TokenService } from '../../services/auth/token.service';
import { AuthService } from '../../services/auth/auth.service';
import { AuthUserService } from '../../services/auth/auth-user.service';
import { User } from '../../models/user';
// import {SnotifyService} from 'ng-snotify';
import {NgForm} from '@angular/forms';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';

@Component({
  selector: 'body',
  templateUrl: 'login.component.html',
  providers: [JwtService],
  animations: [
     trigger('fadeIn', [

       state('out', style({
       	display: 'none',
       	opacity: 0
       })),

       state('in', style({
       	opacity: 1
       })),

       transition('out <=> in', [
         animate(400)
       ]),
     ])
   ]
})

export class LoginComponent
{
	public form: any;
	public error: any;
	public submitTextBtn: string;
	public fadeIn: string = 'out'; 

	@ViewChild('loginForm')
	private loginForm:NgForm;

	constructor(
		private _jwtService:JwtService,
		private _tokenService:TokenService,
		private _authService:AuthService,
		private _authUserService:AuthUserService,
		private router:Router,
	)
	{
		this.form = {
			email: null,
			password:null,
		};

		this.error = false;

		this.submitTextBtn = "Login";
	}

	onSubmit()
	{
		if (this.submitTextBtn === "Espere...")
			return;
		
		this.submitTextBtn = 'Espere...';

		return this._jwtService.login(this.form).subscribe(
			response => {
				this._authService.login(response);
				this.loginForm.reset();
			},
			error => this.handleError(error)
		);
	}





	handleError(error)
	{
		console.log(error);

		if (error.status === 500 || error.status === 0)
			this.error = "Verifique su conexion";
		else
			this.error = error.error.error;

		this.fadeIn = 'in';

		this.submitTextBtn = "Login";

		setTimeout(() => this.fadeIn = "out", 5000);
	}

}
