import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';
import { AssignModalComponent } from './assign-modal/assign-modal.component';

import { formatDate, now, showPreconfirmMessage } from '../../shared/helpers';
import { MovieService as CRUDService } from '../../services/movie.service';
import { ShiftService } from '../../services/shift.service';

import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';


@Component({
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements AfterViewInit {



  @ViewChild(CreationModalComponent)
  public creattionModal: CreationModalComponent;



 @ViewChild(AssignModalComponent)
  public assignModal: AssignModalComponent;

  @ViewChild(TableComponent)
  public table: TableComponent;

  constructor(
    private _shiftService:ShiftService,
    private _crudService:CRUDService,
    private toast: ToastrService,
    private fb: FormBuilder
  //  private spinner: NgxSpinnerService
  ) { }

  ngAfterViewInit(): void {
/*
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 5
*/

    this.table.init();
  }

  public reloadTable(): void {
    this.table.reload();
  }



  public showAssignModal(event?:any):void{

 this._shiftService.get().subscribe(data=>{


    this.assignModal.form_model.form.controls['_method'].setValue('POST');
    this.assignModal.form_model.form.controls['movie_id'].setValue(event.id);  
    this.assignModal.shifts=data;
    this.assignModal.modal.show();

      })
 

    

   

  
  }


  public showCreationModal(event?:any):void{


    if(typeof event !== 'undefined')
    {

      //getting user
      this._crudService.show(event.id).subscribe(data=>{


         this.creattionModal.form_model.form.patchValue({
          _method: 'PUT',
          name:data.movie.name,
          publication_date:data.movie.publication_date,
          status:data.movie.status,
          cover:data.movie.image,
          movie_id:event.id
        });

                   this.creattionModal.imgFile='ededed'


          if(data.movie.image==null)
          {
            this.creattionModal.imgFile=''

          }else
          {
            this.creattionModal.imgFile=this.creattionModal.baseUrl+'/'+data.movie.image;

          }


         this.creattionModal.modal.show();

      })


    }else
    {
    this.creattionModal.form_model.form.controls['_method'].setValue('POST');
    this.creattionModal.form_model.form.controls['movie_id'].setValue(null);  
    this.creattionModal.form_model.form.controls['cover'].setValue('');  
    this.creattionModal.imgFile=null

    this.creattionModal.clean();

    this.creattionModal.modal.show();

    }

   

  
  }



    public delete(event:any):void
  {
    showPreconfirmMessage(
      "¿Eliminar Pelicula?",
      ""
    )
    .then(result => {

      if (result.value)
      {
        this._crudService.delete(event.id).subscribe(
          response => this.handleResponseOfRequest(response));
      }

    });
  }




  private handleResponseOfRequest(response:any):void
  {
    this.toast.success(response.message, 'Exito!');
    this.reloadTable();
  }

}
