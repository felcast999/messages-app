import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { formatDate } from '../../../shared/helpers';
import {environment} from '../../../../environments/environment';

@Component({
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.scss']
})
export class DetailsViewComponent implements OnInit {

	public movie;
  public baseUrl:string;
  public imgSrc:string;


  constructor(
  	private route:ActivatedRoute
  ) { 

    this.baseUrl = environment.APIStorage;

  }

  ngOnInit()
  {
  	this.route.data.subscribe((data:any) =>{ 
      this.movie = data.model.movie
      if(this.movie.image!=null)
      {
        this.imgSrc=this.baseUrl+'/'+this.movie.image;

      }else{
       this.imgSrc=''; 
      }
    });
  }

  public formatDate(date:string|Date, format?:string):string
  {
  		return formatDate(date,format);
  }
}  
