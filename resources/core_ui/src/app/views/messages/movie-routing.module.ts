import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovieComponent } from './movie.component';
import { DetailsViewComponent } from './details-view/details-view.component';

// resolvers
import { ShowResolverService } from '../../services/solvers/movie/show-resolver.service';


const routes: Routes = [{
  path: '',
  data: {
    title: 'Peliculas'
  },
  children: [
    
    {
      path: '',
      component: MovieComponent,
      data: {
        title: "Peliculas"
      }
    },
    {
      path: ':id',
      component: DetailsViewComponent,
      data: {
        title: "Detalles de Pelicula"
      },
      resolve: {
        model: ShowResolverService
      }
    }
  ] 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ShowResolverService
  ]
})
export class MovieRoutingModule { }
