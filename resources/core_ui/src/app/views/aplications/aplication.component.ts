import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';
import { formatDate, now, showPreconfirmMessage } from '../../shared/helpers';
import { AplicationService as CRUDService } from '../../services/aplication.service';
import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';


@Component({
  templateUrl: './aplication.component.html',
  styleUrls: ['./aplication.component.scss']
})
export class AplicationComponent implements AfterViewInit {



  @ViewChild(CreationModalComponent)
  public creattionModal: CreationModalComponent;



  @ViewChild(TableComponent)
  public table: TableComponent;

  constructor(
    private _crudService:CRUDService,
    private toast: ToastrService,
    private fb: FormBuilder
  //  private spinner: NgxSpinnerService
  ) { }

  ngAfterViewInit(): void {
/*
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 5
*/

    this.table.init();
  }

  public reloadTable(): void {
    this.table.reload();
  }

  public showCreationModal(event:any):void{


    if(typeof event !== 'undefined')
    {

      //getting user
      this._crudService.show(event.id).subscribe(data=>{


         this.creattionModal.form_model.form.patchValue({
          _method: 'PUT',
          name:data.product.name,
          aplication_id:event.id
        });

         this.creattionModal.modal.show();

      })


    }else{
    this.creattionModal.form_model.form.controls['_method'].setValue('POST');
    this.creattionModal.form_model.form.controls['aplication_id'].setValue(null);  
    this.creattionModal.modal.show();

    }

   

  
  }



    public delete(event:any):void
  {
    showPreconfirmMessage(
      "¿Eliminar Aplicación?",
      ""
    )
    .then(result => {

      if (result.value)
      {
        this._crudService.delete(event.id).subscribe(
          response => this.handleResponseOfRequest(response));
      }

    });
  }




  private handleResponseOfRequest(response:any):void
  {
    this.toast.success(response.message, 'Exito!');
    this.reloadTable();
  }

}
