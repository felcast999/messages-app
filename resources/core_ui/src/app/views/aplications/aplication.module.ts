import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AplicationRoutingModule } from './aplication-routing.module';

import { AplicationComponent } from './aplication.component';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';
import { DetailsViewComponent } from './details-view/details-view.component';

import { AplicationService } from '../../services/aplication.service';
import {  AuthInterceptor } from '../../services/http-interceptors/auth-interceptor.service';


import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { DataTablesModule } from 'angular-datatables';
import { HttpClient, HttpHeaders, HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  declarations: [
    AplicationComponent,
    CreationModalComponent,
    TableComponent,
    DetailsViewComponent,
    ],
  imports: [
    AplicationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,

  ],
  providers: [
    AplicationService,
    { 
   provide: HTTP_INTERCEPTORS,
   useClass: AuthInterceptor, 
   multi: true 
    }
  ]
})
export class AplicationModule { }
