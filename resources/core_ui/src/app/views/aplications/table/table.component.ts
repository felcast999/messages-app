import { Component, OnInit, OnDestroy, ViewChild, EventEmitter, Output  } from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import { AppDataService } from '../../../services/app-data.service';
import { Aplication } from '../../../interfaces/aplication';
import { AplicationService } from '../../../services/aplication.service';



import { Router } from '@angular/router';

@Component({
  selector: 'table-component',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [
    AppDataService,
  ]
})
export class TableComponent implements OnInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: any = {}; 
  dtTrigger: Subject<any> = new Subject(); 
      
  @Output() 
  public showCreationModal: EventEmitter<any> = new EventEmitter()

      
  @Output() 
  public delete: EventEmitter<any> = new EventEmitter()


  public dats: Array<Worker> = []; 

  private dataSubscription: Subscription; 
  
  constructor(
    private _appDataService: AppDataService,
    private _aplicationService: AplicationService,
    private router:Router

   )
  {
  }

  ngOnInit():void
  {
    this.setDtOptions();
  }

  ngOnDestroy():void
  {   
    this.dtTrigger.unsubscribe();
    this.dataSubscription.unsubscribe();
  }

  public init():void
  {
    // las opciones de la tabla vienen de una peticion http y se debe 
    // esperar a que se resuelva la promesa.
    setTimeout(() => this.dtTrigger.next(), 1000);
  }
  
  public reload(): void
  { 
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload(null, false);
    });
  }

  public async setDtOptions():Promise<void>
  {
    const that = this;

    this.dtOptions = await this._appDataService.getDatatableSettings(); 

    Object.assign(this.dtOptions, {
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      stateSave: true,
      stateDuration: -1,
      ajax:(dataTablesParameters: any, callback) => {
       
        that.dataSubscription = that._aplicationService.getDataTable(dataTablesParameters)
            .subscribe(response => {

              this.dats = response.data; 

            callback({
              recordsTotal: response.recordsTotal,
              recordsFiltered: response.recordsFiltered,
              data: response.data
            });

          });
      },
      columns:[
        {data: '_name',           name: 'name' ,            responsivePriority: 1,  title: "Aplicacion",              width:"15%", searchable: true, orderable: true},
        {data: '_created_at',     name: 'created_at',       responsivePriority: 7,  title: "Registro",            width:"15%", searchable: true, orderable: true, type: "date"},
        {data: '_actions', responsivePriority: 2,  title: "Acciones",      width:"15%", searchable: false, orderable: false},
      ],
      order:[[0,'desc']],

    });

  }


 public emitEvents(event:any):void
  {    
    const target = event.target; 

   
      event.preventDefault();

      let id = target.getAttribute('data-id') ?
                target.getAttribute('data-id') :
                target.parentElement.getAttribute('data-id');


      switch(true)
      {
            case target.classList.contains('delete-model'):
 
            this.delete.emit({id});
          
            break;

            case target.classList.contains('update-model'):
 
            this.showCreationModal.emit({id:id,method:'PUT'});
          
            break;
                    
            case target.classList.contains('details-model'):

            this.router.navigate(['/aplications', id]);
          
            break;

      }

    
  }


   
}
