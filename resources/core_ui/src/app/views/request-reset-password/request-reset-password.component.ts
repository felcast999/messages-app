import { Component, OnInit } from '@angular/core';
import { JwtService } from '../../services/auth/jwt.service';
import {SnotifyService} from 'ng-snotify';
import {Observable} from 'rxjs';

@Component({
  selector: 'body',
  templateUrl: './request-reset-password.component.html',
  styleUrls: ['./request-reset-password.component.css'],
  providers: [JwtService]
})
export class RequestResetPasswordComponent implements OnInit {

	public form: any;
  public btnText = 'Enviar correo de recuperacion'; 

  constructor(
  	private _jwtService: JwtService,
  	private notify: SnotifyService
  )
  {
  	this.form = {
  		email: ''
  	};
  }

  ngOnInit(): void {
  }

  onSubmit()
  {

    if (this.btnText === 'Espere...')
      return;

    this.btnText = 'Espere...';

    const action = Observable.create(observer => {
          
          observer.next({
            title: 'Espere...',
            body: 'Enviando solicitud...',
          });

          this._jwtService.sendResetPasswordLink(this.form).subscribe(
            response => this.handleResponse(response, observer),
            error => this.handleError(error, observer)
          );
       
        });

     this.notify.async('',action,{ 
         closeOnClick: true,
       });

  }

  handleResponse(data, observer)
  {
    observer.next({
      title: 'Exito!',
      body: data.original.message
    });

    observer.complete();
    
    this.btnText = 'Enviar correo de recuperacion'; 
  }

  handleError(error, observer)
  {
      const errors = error.error.errors;
      
      if (error.status === 500 || error.status === 0)
      {
        observer.error({
            title: 'Error',
            body: 'Verifique su conexion',
        });
      }
      else
      {
        observer.error({
            title: 'Error',
            body: errors.email,
        });

      }

      this.btnText = 'Enviar correo de recuperacion'; 

  }
}
