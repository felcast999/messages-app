import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { JwtService } from '../../services/auth/jwt.service';
import { TokenService } from '../../services/auth/token.service';
import { AuthService } from '../../services/auth/auth.service';
import { AuthUserService } from '../../services/auth/auth-user.service';
import { User } from '../../models/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html',
  providers: [JwtService]
})

export class RegisterComponent implements OnInit, OnDestroy {

	public form: any;
  public errors: any;

  constructor(
    private _jwtService: JwtService,
    private _tokenService: TokenService,
    private _authService: AuthService,
    private _authUserService: AuthUserService,
    private router: Router,
  	)
  {
  	this.form = {
  		first_name: null,
  		last_name: null,
      email: null,
  		password: null,
  		password_confirmation: null
  	}; 

  	this.errors = false;
  }

  ngOnInit(): void 
  {
   
  }

  ngOnDestroy(): void
  {

  }

  onSubmit()
  {

    const submitBtn:any = document.querySelector('button[type="submit"');
    submitBtn.textContent = 'Espere...';
    submitBtn.disabled = true;

  	return this._jwtService.register(this.form).subscribe(
  		response => {
  			this.handleResponse(response);
  		},
  		error => {
  			this.handleError(error);
        submitBtn.textContent = 'Login';
        submitBtn.disabled = false;
  		}
  	);
  }

  handleResponse(data)
  {
    this._tokenService.handle(data.access_token);
    this._authService.changeAuthStatus(true);
    
    const {id, first_name, last_name, email, profile_pic,is_active,roles} =  data.user.original,
       authUser = new User(id, first_name, last_name, email, profile_pic,is_active,roles);
    
    localStorage.setItem('authUser', JSON.stringify(authUser));
    this._authUserService.updateAuthUser(authUser);

    this._tokenService.handle(data.access_token);
    this.router.navigate(['/dashboard']);

  }

  handleError(error)
  {
  	
    if (error.status === 0  || error.status === 500){
      this.errors = ["Verifique su conexion"];
    }
    else{
      const errors = error.error.errors;
    	this.errors = [];
  	  Object.values(errors).forEach((_error:string[]) => this.errors.push(..._error));
    }
  
  }

}
