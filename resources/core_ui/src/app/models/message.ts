export class Message {
   
constructor(
		public id:number,
		public user_id :number ,
		public subject:string,
		public receiver:string,
		public content:string,
		public status_id :number ,
		public created_at?:string,
		public updated_at?:string,
	)
	{

	}


/* Methods */

	static createInstance(data:any)
	{
		const {id,user_id,subject,receiver,content,status_id,created_at, updated_at} = data;
		return new Message(id,user_id,subject,receiver,content,status_id,created_at, updated_at);   
	}


}
