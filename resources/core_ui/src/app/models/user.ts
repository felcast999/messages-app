export class User
{	
	constructor(
		public id: number,
		public document_number: string,
		public first_name: string,
		public last_name: string,
		public email?: string,
		public profile_pic?: string,
		public phone?: number,
		public roles?: Array<any>, // roles 
		public departments?: Array<any>, // departamentos 
		public created_at?:string,
		public updated_at?:string
	)
	{

	}

	public static createInstance(data): User
	{
		const {id, first_name, last_name, email, profile_pic, is_active, roles, created_at} =  data,
		       instance = new User(id, first_name, last_name, email, profile_pic, is_active, roles, created_at);

		return instance;     
	}

	/* getters */

	get photoName():string
	{
		return this.profile_pic.substring(this.profile_pic.indexOf('_') + 1);
	}

	/* methods */

	public hasRoles() :boolean
	{
		return this.roles.length ? true : false;
	}

	public hasDepartments() :boolean
	{
		return this.departments.length ? true : false;
	}

	public isAdmin():boolean
	{
		const rolesOfThisUser = this.getRolesOfThisUser();
		return rolesOfThisUser.includes("admin");
	}

	private getRolesOfThisUser() : Array<string>
	{
	    return this.roles.map((role:any) => role.name);
	}

	private getDepartmentsOfThisUser() : Array<string>
	{
	    return this.departments.map((department:any) => department.name);
	}

	public hasOneOfTheseRoles(...roles:any): boolean
	{
		if (this.hasRoles())
		{
			const requiredRoles = Array.isArray(roles[0]) ? roles[0] : Array.from(arguments),
					rolesOfThisUser = this.getRolesOfThisUser();

			for(let requiredRole of requiredRoles)
			{
		    	if (rolesOfThisUser.includes(requiredRole))
		    	    return true;
			}

			return false;
		}

		return false;
	}

	public hasOneOfTheseDepartments(...departments:any): boolean
	{
		if (this.hasDepartments())
		{
			const requiredDepartments = Array.isArray(departments[0]) ? departments[0] : Array.from(arguments),
					departmentsOfThisUser = this.getDepartmentsOfThisUser();

			for(let requiredDepartment of requiredDepartments)
			{
		        if (departmentsOfThisUser.includes(requiredDepartment))
		            return true;
			}

		    return false;   
		}

		return false;
	}

	canDoInstallations():boolean
	{
		return this.hasOneOfTheseRoles('Operario') && 
				this.hasOneOfTheseDepartments('INSTALACIONES', 'OPERACIONES', 'MANTENIMIENTO'); 
	}

}
