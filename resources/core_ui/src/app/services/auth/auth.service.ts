import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router, UrlTree } from '@angular/router';
import { User } from '../../models/user';
import {environment} from '../../../../environments/environment';

import { TokenService } from './token.service';
import {AuthUserService} from './auth-user.service';
import swal from "sweetalert2";

@Injectable()

export class AuthService {

	private loggedIn;
	public authStatus;
	public redirectUrl:string;

	constructor(
		private _http: HttpClient,
		private _tokenService: TokenService,
		private _authUserService: AuthUserService,
		private router: Router,
		)
	{
		this.loggedIn = new BehaviorSubject<boolean>(this._tokenService.loggedIn());
		this.authStatus = this.loggedIn.asObservable();
	}

	public setAuthenticatedUserToInitApp():() => Promise<any>
	{
		if (localStorage.getItem('token'))
		{
		  const token = localStorage.getItem('token'),
		        httpHeaders = {
		          headers: new HttpHeaders({
		            'Authorization': `Bearer  ${token}`
		          })
		        };

		  return () => {
		    
		    return this._http.get(`${environment.host}/me`, httpHeaders)
		              .toPromise()
		              .then((data:any) => {
		              	this._authUserService.updateAuthUser(User.createInstance(data))
		              })
		              .catch(error => console.log(error));
		  }

		}

		return () => {

			return new Promise((resolve, reject) => {
			  resolve('no session started');
			})
			.then((message:string) => console.log(message));
		}
	}

	public login(response:any):Promise<boolean>
	{
		this._tokenService.handle(response.data.access_token);

		this.changeAuthStatus(true);
		
		const loggedUser = this.setAndReturnAuthenticatedUserToAppAfterToLogin(response.data.user.data);

		return this.redirectUserToDefaultPathBasedOnTheirRoles(loggedUser);
	}

	public changeAuthStatus(value:boolean):void
	{
		this.loggedIn.next(value);
	}

	private setAndReturnAuthenticatedUserToAppAfterToLogin(userData:any):User
	{
		const authenticatedUser:User = User.createInstance(userData);
		this._authUserService.updateAuthUser(authenticatedUser);

		return authenticatedUser;
	}

	public redirectUserToDefaultPathBasedOnTheirRoles(loggedUser:User):Promise<boolean>
	{
		if (loggedUser.hasOneOfTheseRoles(['admin','Jefe']))
		{
			this.redirectUrl = '/dashboard';
		}
		else
		{
			this.redirectUrl = loggedUser.hasOneOfTheseDepartments([
											'INSTALACIONES',
											'OPERACIONES',
											'MANTENIMIENTO'
											]) ?
										'/ticket_jobs' :
										'/tickets';
		}

		return this.router.navigateByUrl(this.redirectUrl);
	}

	public closeSession(title:string, message:string, type:any):void
	{
		this.router.navigateByUrl('/login');
		this.redirectUrl = "";
		this.changeAuthStatus(false);
		this._tokenService.removeToken();
		this._authUserService.removeAuthUser();
		swal.fire(title,message,type);
	}

}
