import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs';
import {TokenService} from './token.service';
import {JwtService} from './jwt.service';
import {AuthService} from './auth.service';
import {AuthUserService} from './auth-user.service';
import {User} from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class AfterLoginService implements CanActivate, CanActivateChild {

	private authenticatedUser:User;

	constructor(
		private _authService: AuthService,
		private _authUserService: AuthUserService,
		private _tokenService: TokenService,
		private _jwtService: JwtService,
		private router: Router,
	)
	{
		this._authUserService.getAuthUser().subscribe(
			(authenticatedUser:User) => this.authenticatedUser = authenticatedUser
		);
	}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>
	{	
		return !this._tokenService.loggedIn() ?
				this.redirectToLogin(state) :
				this.checkIfHasPermissionToAccessTheRoute(route);
	}

	canActivateChild(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>
	{
		return this.canActivate(route, state);
	}

	private redirectToLogin(state:RouterStateSnapshot):boolean | Promise<boolean>
	{
		this._authService.redirectUrl = state.url;
		
		this.router.navigate(['login']);
		
		return this._tokenService.loggedIn();
	}

	private checkIfHasPermissionToAccessTheRoute(route:ActivatedRouteSnapshot):boolean | Promise<boolean>
	{
		if (this.authenticatedUser && !this.authenticatedUser.isAdmin())
		{
			return this.hasPermissionToAccessTheRoute(route) ?
			true:
			this._authService.redirectUserToDefaultPathBasedOnTheirRoles(this.authenticatedUser);

		}

		return true;
	}

	private hasPermissionToAccessTheRoute(route:ActivatedRouteSnapshot):boolean
	{
		const allowed_roles:string|string[] = route.data.allowed_roles,
			 allowed_departments:string|string[] = route.data.allowed_departments,
			 hasRolesToAccessTheModule:boolean = allowed_roles === "all" ? true : this.authenticatedUser.hasOneOfTheseRoles(allowed_roles),
			 hasDepartmentsToAccessTheModule:boolean = allowed_departments === "all" ? true : this.authenticatedUser.hasOneOfTheseDepartments(allowed_departments);
		
		return hasRolesToAccessTheModule && hasDepartmentsToAccessTheModule;
	}

}
