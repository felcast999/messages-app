import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Role } from '../models/role';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

	public url: string;
	private roles$: BehaviorSubject<Array<any>>;
	public allRoles: Observable<Array<any>>;

  constructor(
  	private http: HttpClient
  )
  {
  	this.roles$ = new BehaviorSubject<any[]>([]);
  	this.allRoles = this.roles$.asObservable();
  	this.url = environment.host;
  }

  updateRoles(value:any[])
  {
    this.roles$.next(value);
  }

  setAuthenticationHeaders(): object
  {
      const token = localStorage.getItem('token'),
            httpHeaders = {
              headers: new HttpHeaders({
                'Authorization': `Bearer  ${token}`
              })
            };

      return httpHeaders;
  }

  getRoles(): Observable<any>
  {
     const httpHeaders = this.setAuthenticationHeaders();

    return this.http.get(`${this.url}/roles`, httpHeaders);
  }

  // createRole(data)
  // {
  //    const httpHeaders = this.setAuthenticationHeaders();

  //   return this.http.post(`${this.url}/parameters/roles`,data, httpHeaders);
  // }

  // editRole(id)
  // {
  //    const httpHeaders = this.setAuthenticationHeaders();

  //   return this.http.get(`${this.url}/parameters/roles/${id}`, httpHeaders);
  // }

  // updateRole(data, id)
  // {
  //    const httpHeaders = this.setAuthenticationHeaders();

  //   return this.http.put(`${this.url}/parameters/roles/${id}`,data, httpHeaders);
  // }

  // deleteRole(id)
  // {
  //    const httpHeaders = this.setAuthenticationHeaders();

  //   return this.http.delete(`${this.url}/parameters/roles/${id}`, httpHeaders);
  // }

}
