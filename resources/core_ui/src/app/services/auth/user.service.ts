import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../../models/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  public url: string;
  private users$: BehaviorSubject<Array<any>>;
  public allUsers: Observable<Array<any>>;

  constructor(
    private http: HttpClient
  )
  {
    this.url = environment.host;
    this.users$ = new BehaviorSubject<Array<any>>([]);
    this.allUsers = this.users$.asObservable();
  }

  updateUsers(value:User[])
  {
    this.users$.next(value);
  }

  setAuthenticationHeaders(): object
  {
      const token = localStorage.getItem('token'),
            httpHeaders = {
              headers: new HttpHeaders({
                'Authorization': `Bearer  ${token}`
              })
            };

      return httpHeaders;
  }

  getUsers(): Observable<any>
  {
   
     const httpHeaders = this.setAuthenticationHeaders();

    return this.http.get(`${this.url}/users`, httpHeaders);
  }

  createUser(data)
  {
     const httpHeaders = this.setAuthenticationHeaders();

    return this.http.post(`${this.url}/users`,data, httpHeaders);
  }

  editUser(id)
  {
     const httpHeaders = this.setAuthenticationHeaders();

    return this.http.get(`${this.url}/users/${id}`, httpHeaders);
  }

  updateUser(data, id)
  {
     const httpHeaders = this.setAuthenticationHeaders();

    return this.http.post(`${this.url}/users/${id}`,data, httpHeaders);
  }

  deleteUser(id)
  {
     const httpHeaders = this.setAuthenticationHeaders();

    return this.http.delete(`${this.url}/users/${id}`, httpHeaders);
  }

  usersByDepartment(department:string)
  {
     const httpHeaders = this.setAuthenticationHeaders();

    return this.http.get(`${this.url}/users/department/${department}`, httpHeaders);

  }

}
