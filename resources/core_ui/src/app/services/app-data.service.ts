import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AppDataService {

  constructor(
    private http:HttpClient
  ) { }

  public getDatatableSettings():Promise<any>
  {
    return this.http.get('assets/app-data/datatables-settings.json').toPromise();
  }
}
