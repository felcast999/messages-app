import { Injectable } from '@angular/core';
import {
	Resolve,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';

import { UserService } from '../../user.service';
import { User } from '../../../models/user';
import { map,catchError } from 'rxjs/operators';

@Injectable()
export class ShowResolverService implements Resolve<any>
{
  constructor(
  	private _userService:userService
  ) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Shift|any>
  {
      const id = Number(route.paramMap.get('id'));

  		return this._userService
  					   .show(id);
  }





}

