import { Injectable } from '@angular/core';
import {
	Resolve,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';

import { MessageService } from '../../message.service';
import { Message } from '../../../models/movie';
import { map,catchError } from 'rxjs/operators';

@Injectable()
export class ShowResolverService implements Resolve<any>
{
  constructor(
  	private _messageService:MessageService
  ) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Movie|any>
  {
      const id = Number(route.paramMap.get('id'));

  		return this._messageService
  					   .show(id);
  }





}

