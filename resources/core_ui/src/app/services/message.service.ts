import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Movie } from '../models/movie';

@Injectable()
export class MovieService {

	private baseUrl:string;

	constructor(
		private _http: HttpClient
		)
	{
		this.baseUrl = environment.APIEndpoint;
	}


	setAuthenticationHeaders(): object
	{
		const token = localStorage.getItem('token'),
		httpHeaders = {
			headers: new HttpHeaders({
				'Authorization': `Bearer  ${token}`
			})
		};

		return httpHeaders;
	}

	public delete(id:number):Observable<any>
	{
		return this._http.delete(`${this.baseUrl}/movies/${id}`);
	}


    public getDataTable(dataTablesParameters: object): Observable<any>
	{
		return this._http.post(`${this.baseUrl}/datatables/movies`, dataTablesParameters);
	}

	public store(data:any):Observable<any>
	{
		//const httpHeaders = this.setAuthenticationHeaders();

		return this._http.post(`${this.baseUrl}/movies`, data);
	}



	public assign(data:any):Observable<any>
	{
		//const httpHeaders = this.setAuthenticationHeaders();

		return this._http.post(`${this.baseUrl}/movies/shift-assign`, data);
	}

	

	public update(data:any):Observable<any>
	{
		//const httpHeaders = this.setAuthenticationHeaders();

		return this._http.post(`${this.baseUrl}/movies/${data.movie_id}`, data);
	}


	public show(id:number):Observable<any>
	{
    return this._http
              .get(`${this.baseUrl}/movies/${id}`);
	}
	




}

