import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Client } from '../models/client';

@Injectable()
export class ClientService {

	private baseUrl:string;

	constructor(
		private _http: HttpClient
		)
	{
		this.baseUrl = environment.APIEndpoint;
	}


	setAuthenticationHeaders(): object
	{
		const token = localStorage.getItem('token'),
		httpHeaders = {
			headers: new HttpHeaders({
				'Authorization': `Bearer  ${token}`
			})
		};

		return httpHeaders;
	}

	public delete(id:number):Observable<any>
	{
		return this._http.delete(`${this.baseUrl}/clients/${id}`);
	}


    public getDataTable(dataTablesParameters: object): Observable<any>
	{
		return this._http.post(`${this.baseUrl}/datatables/clients`, dataTablesParameters);
	}

	public store(data:any):Observable<any>
	{
		//const httpHeaders = this.setAuthenticationHeaders();

		return this._http.post(`${this.baseUrl}/clients`, data);
	}


	public update(data:any):Observable<any>
	{
		//const httpHeaders = this.setAuthenticationHeaders();

		return this._http.post(`${this.baseUrl}/clients/${data.client_id}`, data);
	}


	public show(id:number):Observable<any>
	{
    return this._http
              .get(`${this.baseUrl}/clients/${id}`);
	}
	

}

