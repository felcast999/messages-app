import { INavData } from '@coreui/angular';

export const navItems: INavData[] = 
[
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-home',
    attributes: {
        allowed_roles: 'admin'
    }
  },
  {
    name: 'Peliculas',
    url: '/movies',
    icon: 'fa fa-film',
    attributes: {
        allowed_roles: ['admin','super admin','worker','user']
    }
  },
  {
    name: 'Turnos',
    url: '/shifts',
    icon: 'fa fa-ticket',
    attributes: {
        allowed_roles: ['admin','super admin','worker','user']
    }
  }
];
