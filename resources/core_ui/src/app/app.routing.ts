import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';


import { RequestResetPasswordComponent } from './views/request-reset-password/request-reset-password.component';
import { ResponsePasswordResetComponent } from './views/response-password-reset/response-password-reset.component';

import {BeforeLoginService} from './services/auth/before-login.service';
import {AfterLoginService} from './services/auth/after-login.service';





export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate:[BeforeLoginService],
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate:[BeforeLoginService],
    data: {
      title: 'Register Page'
    }
  },
  {
  path: 'request-reset-password',
  component: RequestResetPasswordComponent,
  canActivate:[BeforeLoginService]
},
{
  path: 'response-password-reset',
  component: ResponsePasswordResetComponent,
  canActivate:[BeforeLoginService]
},
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate:[AfterLoginService],
    data: {
      title: 'Home'
    },
    children: [
      {
       path: 'movies',
       loadChildren: () => import('./views/movies/movie.module').then(m => m.MovieModule),
        canActivate:[AfterLoginService],
        data: {
         allowed_roles: ['all'],
       }

      },
       {
       path: 'shifts',
       loadChildren: () => import('./views/shifts/shift.module').then(m => m.ShiftModule),
        canActivate:[AfterLoginService],
        data: {
         allowed_roles: ['all'],
       }

      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
