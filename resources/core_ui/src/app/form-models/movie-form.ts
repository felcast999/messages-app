import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';


export class MovieForm {
 
 public form:FormGroup;



constructor(
		    private fb?: FormBuilder
	)
	{

     this.form = this.fb.group({
      _method:new FormControl('POST'),
      movie_id:new FormControl(null),
      name: new FormControl('',Validators.required),
      publication_date: new FormControl('',Validators.required),
      status: new FormControl('',Validators.required),
      cover: new FormControl(''),

    });

	}



 get model_form_control() {
    return this.form.controls;
  }



  get is_invalid() {
    return this.form.invalid;
  }
  


}
