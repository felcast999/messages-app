import {SweetAlertOptions,SweetAlertResult} from 'sweetalert2/dist/sweetalert2.js'
import swal from 'sweetalert2';

export function formatDate(_date:Date|string, format?): string
{
	const date = _date instanceof Date ? _date : new Date(_date);

	const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate(),
	month = date.getMonth() + 1 < 10 ? `0${date.getMonth()+1}` : date.getMonth()+1,
	year = date.getFullYear();

	const hour = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours(),
		  minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();

	let formatDate;

	switch (format)
	{
		case "Y-m-d H:i":
		formatDate = `${year}-${month}-${day} ${hour}:${minutes}`;
		break;

		case "Y-m-d":
		formatDate = `${year}-${month}-${day}`;
		break;
		
		case "Y/m/d":
		formatDate = `${year}/${month}/${day}`;
		break;
		
		default:
		formatDate = `${day}/${month}/${year} ${hour}:${minutes}`;
		break;
	}

	return formatDate;
}

export function formatTime(_time:Date|string, format:string="H:i"): string
{
	const time = _time instanceof Date ? _time : new Date(_time);
	let hour, minutes;

	hour = time.getHours() < 10 ? `0${time.getHours()}` : time.getHours(),
	minutes = time.getMinutes() < 10 ? `0${time.getMinutes()}` : time.getMinutes();

	if (format === "H:i")
		return `${hour}:${minutes}`;
}

// export function toggleFullscreenImage(e):void 
// {
// 	let elem = e.target,
// 	imgSrc = elem.getAttribute('src');

// 	if (imgSrc.includes("default.png") || imgSrc.includes('no-photo.png') || !imgSrc)
// 		return;

// 	if (!document.fullscreenElement) {
// 		elem.requestFullscreen().catch(err => {
// 			alert(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
// 		});
// 	} else {
// 		document.exitFullscreen();
// 	}
// } 

export async function getImageDataOfFileInpuEvent(event):Promise<any>
{
	let image = event.target.files[0];

	const processingImage = new Promise((resolve, reject) => {

		if (!checkIfTheFileIsAImage(image))
			reject('image no valid');

		const reader = new FileReader();

		reader.readAsDataURL(image); 

		reader.onload = (_event) => {
			resolve({
				image: image,
				url: reader.result
			});
		}

	});

	return await processingImage;
}

function checkIfTheFileIsAImage(image):boolean
{
	return image.type === 'image/png' || 
			image.type === 'image/jpeg' ||
			image.type === 'image/jpg';
}

export async function showPreconfirmMessage(
		title:string,
		text:string,
		type:string = "warning",
		cancelButtonText:string = "Cancelar",
		confirmButtonText:string = "Eliminar",
	):Promise<SweetAlertResult>
{
	const swalOptions = {
		title: title,
		text: text,
		type: type,
		showCancelButton: true,
		confirmButtonColor: "#d33",
		cancelButtonColor: "#3085d6",
		cancelButtonText: cancelButtonText,
		confirmButtonText: confirmButtonText,
	} as SweetAlertOptions;

	return await swal.fire(swalOptions);
}

export function parseToUrlParams(params:object):string
{
	let formatedParams = '?';

	Object.keys(params).forEach(key=> formatedParams += `${key}=${params[key]}&`); 

	return formatedParams;
}

export function now():Date
{
	return new Date();
}