import { BrowserModule } from '@angular/platform-browser';
import { NgModule,APP_INITIALIZER,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {environment} from '../environments/environment';
import { HttpClient, HttpHeaders, HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';

import { JwtModule } from "@auth0/angular-jwt";


import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { IconModule, IconSetModule, IconSetService } from '@coreui/icons-angular';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';


import { AuthService } from './services/auth/auth.service';
import { TokenService } from './services/auth/token.service';
import { AfterLoginService } from './services/auth/after-login.service';
import { BeforeLoginService } from './services/auth/before-login.service';
import { AuthUserService } from './services/auth/auth-user.service';
import {  AuthInterceptor } from './services/http-interceptors/auth-interceptor.service';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

import { RequestResetPasswordComponent } from './views/request-reset-password/request-reset-password.component';
import { ResponsePasswordResetComponent } from './views/response-password-reset/response-password-reset.component';





const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';


import { ToastrModule } from 'ngx-toastr';



export function initApp(_authService: AuthService):() => any
{
  return _authService.setAuthenticatedUserToInitApp();
}


export function tokenGetter() {
  return localStorage.getItem("access_token");
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    IconModule,
    IconSetModule.forRoot(),
    SweetAlert2Module,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["http://localhost:4200/"],
        disallowedRoutes: ["http://example.com/examplebadroute/"],
      },
    }),
    SnotifyModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    RequestResetPasswordComponent,
    ResponsePasswordResetComponent,
  ],
  providers: [
    BeforeLoginService,
    AfterLoginService,
    AuthUserService,
    TokenService,
    AuthService,
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    { 
   provide: HTTP_INTERCEPTORS,
   useClass: AuthInterceptor, 
   multi: true 
    },
    {
    provide: APP_INITIALIZER,
    useFactory: initApp,
    multi: true,
    deps: [
      AuthService
    ]
  },
    IconSetService,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
