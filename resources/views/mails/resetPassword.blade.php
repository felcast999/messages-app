<div style="display: inline-block; margin-right: 25px;">
	<h2 style="color: grey; border-right: 2px solid grey; padding: 25px;"><span style="border: 4px solid grey; padding: 12px; border-radius: 18px;"><b>Control Financiero</b></span></h2>
</div>

<h4 style="color: grey;">Siga el enlace del boton para cambio de contraseña</h4>
<h6 style="color: grey;">(Este enlace puede ser utilizado una sola vez)</h6>

<a href='https://dev-sgfinanciero.osp.pe/response-password-reset?email={{$email}}&token={{$token}}' 
	style="color: #fff;
			text-decoration-line: none;
    		background-color: #0062cc;
    		border-color: #005cbf;
    		cursor: pointer;
    		display: inline-block;
    		font-weight: 400;
    		border: 1px solid transparent;
		    padding: .375rem .75rem;
		    font-size: 0.8rem;
		    line-height: 1.5;
		    border-radius: .25rem;
		    transition: color .15s ease-in-out,background-color .15s ease-in-out,
		    border-color .15s ease-in-out,box-shadow .15s ease-in-out;"
>Cambiar contraseña</a>