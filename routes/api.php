<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');

Route::post('sendResetPasswordLink', 'ResetPasswordController@sendEmail');
Route::post('changePassword', 'ChangePasswordController@process');

Route::middleware('auth:sanctum')->group(function(){

    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');


Route::group([
	'prefix' => 'v1',
	'namespace' => 'API\V1'
], function(){



  Route::post('user-profile', 'admin\UserController@update_profile');

	 Route::resources([

      // usuarios
     'users'=>'admin\UserController',

     //roles
     'roles'=>'admin\RoleController',

      //mensajes
     'messages'=>'MessageController',

  

    ]);




   Route::post('users-destroy-multiple','admin\UserController@destroy_multiple');
   Route::post('roles-destroy-multiple','admin\RoleController@destroy_multiple');
   Route::post('messages-destroy-multiple','MessageController@destroy_multiple');



});
  



});
  
