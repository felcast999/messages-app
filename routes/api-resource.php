<?php

use Illuminate\Support\Facades\Route;

Route::group([
	'prefix' => 'v1/resources',
	'namespace' => 'API\V1\resources',
	'middleware' => ['auth:sanctum'],
], function(){

		#roles
		Route::get('get-roles','RoleController@get');



		#status

		Route::get('get-status-types','StatusController@get_type');
		Route::get('get-status/{type}','StatusController@get');

	

		#departments
		Route::get('get-departments','DepartmentController@get');


	    #provinces
		Route::get('get-provinces','ProvinceController@get');
		Route::get('get-provinces/{id}','ProvinceController@get_by_department');


		#district
		Route::get('get-districts','DistrictController@get');
		Route::get('get-districts/{id}','DistrictController@get_by_province');
		Route::get('get-districts-by-department/{id}','DistrictController@get_by_department');

});
