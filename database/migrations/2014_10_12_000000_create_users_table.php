<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('document_number')->nullable();
            $table->string('first_name',100)->nullable();
            $table->string('last_name',100)->nullable();
            $table->string('profile_pic')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->date('birthdate')->nullable();

            $table->unsignedBigInteger('department_id')->nullable();
      

            $table->foreign('department_id')->references('id')->on('departments')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            

            $table->unsignedBigInteger('province_id')->nullable();
      

            $table->foreign('province_id')->references('id')->on('provinces')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');


            $table->unsignedBigInteger('district_id')->nullable();
      

            $table->foreign('district_id')->references('id')->on('districts')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');


            $table->unsignedBigInteger('status_id')->nullable();
      
            $table->foreign('status_id')->references('id')->on('statuses')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

        
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
