<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Status;
use App\Models\StatusType;

use Illuminate\Support\Facades\DB;
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusType::truncate();
        Status::truncate();

        $type=StatusType::create(["name"=>"system"]);


        $status=["activo","inactivo"];

        foreach ($status as $key => $value) 
        {
        	        Status::create(["name"=>$value,"status_type_id"=>$type->id]);

        }


        $orders=StatusType::create(["name"=>"messages"]);


        $status=["Sent","not sent"];

        foreach ($status as $key => $value) 
        {
                    Status::create(["name"=>$value,"status_type_id"=>$orders->id]);

        }

    

        
        
      
    }
}
