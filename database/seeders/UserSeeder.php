<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;



class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
      public function run()
    {
        User::truncate();
        DB::statement('TRUNCATE TABLE role_user');

        $users = [
            [
                'first_name' => 'Admin',
                'last_name' => 'admin',
                'email' => 'admin@message.com',
                'password' => 'admin'
            ]
        ];

        foreach ($users as $user)
        {
            User::create($user);
        }
        
      

       
    }
}
