<?php

namespace Database\Seeders;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        
        $roles = [
            'super_admin'=>'super administrador',
        	'admin' => 'adminisdrator',
        	'system_user' => 'usuario de sistema'

        ];


        foreach ($roles as $name => $description)
        {
        	$role = Role::create([
		        		'name' => $name,
		        		'display_name' => $name,
		        		'description' => $description
		        	]);

        }
        
        $admin_role = Role::where('name','admin')->first();
        $super_admin_role = Role::where('name','super_admin')->first();

        // admin
        $admin = User::whereEmail('admin@message.com')->first();
        $admin->roles()->attach([$admin_role->id, $super_admin_role->id]);

        
    }
}
