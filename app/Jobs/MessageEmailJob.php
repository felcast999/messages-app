<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\MessageEmail;
use App\Models\Message;

use Mail;

class MessageEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $send_mail;
    protected $message_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($send_mail, $message_id)
    {
        $this->send_mail = $send_mail;
        $this->message_id = $message_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    $message=Message::find($this->message_id);
    try {
        Mail::to($this->send_mail)->send(new MessageEmail($message));
    }
    catch(Exception $e) {
        $this->failed($e);
    }
      
    }

public function failed($exception)
{
    $exception->getMessage();
    // etc...
}

}
