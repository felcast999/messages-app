<?php

namespace App\UseCases\V1\Message;

use App\Models\Message;
use App\Models\Status;
use App\Models\StatusType;

use Illuminate\Http\Request;

class Update
{
    private $model;
    private $status;
    private $status_type;


    public function __construct(
        Message $model,
        Status $status,
        StatusType $status_type,

    )
    {
        $this->model = $model;
        $this->status = $status;
        $this->status_type = $status_type;

    }
    public function execute(Request $request)
    {
        $status_type=$this->status_type->where('name','messages')->first();

        $status=$this->status->where(["status_type_id"=>$status_type->id,"name"=>"not sent"])->first();


        $request->merge([
            "user_id"=>auth()->user()->id,
            "status_id"=>$status->id
        ]);


        $model = $this->model->find($request["message_id"]);

        $model->update($request->all());


        return $model;
    }


   
}