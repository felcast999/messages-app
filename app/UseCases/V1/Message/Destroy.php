<?php

namespace App\UseCases\V1\Message;

use App\Models\Message;
use Illuminate\Http\Request;

class Destroy
{
    private $model;

    public function __construct(
        Message $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {
        return  $this->model->destroy($id);
    }

         public function destroy_multiple(Request $request)
    {
        return  $this->model->whereIn('id',$request->ids)->delete();
    }
}