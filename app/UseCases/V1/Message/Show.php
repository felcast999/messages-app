<?php

namespace App\UseCases\V1\Message;

use App\Models\Message;

class Show
{
    private $model;

    public function __construct(
        Message $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {
        $message = $this->model->findOrFail($id);
        
        return compact('message');
    }
}