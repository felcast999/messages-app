<?php

namespace App\UseCases\V1\Message;

use App\Models\Message;
use App\Models\Status;
use App\Models\StatusType;
use App\Models\User;;

use App\Jobs\MessageEmailJob;

use Illuminate\Http\Request;

class Create
{
    private $model;
    private $status;
    private $status_type;


    public function __construct(
        Message $model,
        Status $status,
        StatusType $status_type,
        User $user

    )
    {
        $this->model = $model;
        $this->status = $status;
        $this->status_type = $status_type;
        $this->user = $user;

    }

    public function execute(Request $request)
    {
        $status_type=$this->status_type->where('name','messages')->first();

        $status=$this->status->where(["status_type_id"=>$status_type->id,"name"=>"not sent"])->first();


        $request->merge([
            "user_id"=>auth()->user()->id,
            "status_id"=>$status->id
        ]);

        $user=$this->user->find(auth()->user()->id);

        $model = $this->model->create($request->all());

        //send email

        dispatch(new MessageEmailJob($user->email,$model->id));

        


        return $model;
    }


   
}