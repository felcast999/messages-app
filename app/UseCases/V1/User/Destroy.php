<?php

namespace App\UseCases\V1\User;

use App\Models\User;
use Illuminate\Http\Request;

class Destroy
{
    private $model;

    public function __construct(
        User $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {
        return  $this->model->destroy($id);
    }


      public function destroy_multiple(Request $request)
    {
        return  $this->model->whereIn('id',$request->ids)->delete();
    }
}