<?php

namespace App\UseCases\V1\User;

use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Carbon;

class Update
{
    private $model;


    public function __construct(
        User $model

    )
    {
        $this->model = $model;

    }

    public function execute(Request $request)
    {
        $model = $this->model->find($request->user_id);


        $model->first_name=$request->first_name;
        $model->last_name=$request->last_name;
        #$model->email=$request->email;
        $model->phone=$request->phone;
        $model->birthdate=$request->birthdate;
        #$model->document_number=$request->document_number;
        $model->department_id=$request->department_id;
        $model->province_id=$request->province_id;
        $model->district_id=$request->district_id;
        $model->status_id=$request->status_id;



         if($request->file('avatar')) 
        {

  if (!empty($request->file('avatar')->getClientOriginalName())) 
            {

            $fileName = time().'_'.$request->file('avatar')->getClientOriginalName();
            $filePath = $request->file('avatar')->storeAs('avatar/'.$model->id, $fileName, 'public');

            #$url = '/storage/' . $filePath;
            $url = $filePath;

        $model->profile_pic=Storage::disk('public')->url($filePath);
        #$model->profile_pic=Storage::disk('s3')->url($filePath);
             }

       


        }



 if ($request->change_password) 
        {
               $model->password=$request->password;

        }
        #$model->update($request->all());
        $model->save();



        $model->roles()->sync($request->role_id);
       
        return $model;
    }


   
}