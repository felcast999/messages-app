<?php

namespace App\UseCases\V1\User;

use App\Models\User;
use App\Models\Role;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Carbon;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Create
{
    private $user;
    private $role;


    public function __construct(
        User $user,
        Role $role

    )
    {
        $this->user = $user;
        $this->role = $role;

    }

    public function execute(Request $request)
    {
        

     

        $user = $this->user->create($request->all());

        if($request->file('avatar')) 
        {
            $fileName = time().'_'.$request->file('avatar')->getClientOriginalName();
            $filePath = $request->file('avatar')->storeAs('avatar/'.$user->id, $fileName, 'public');
            #$filePath = $request->file('avatar')->storeAs('avatar/'.$user->id, $fileName, 's3');

            #$url = '/storage/' . $filePath;
            $url = $filePath;

        $user->avatar=Storage::disk('public')->url($filePath);
        #$user->profile_pic=Storage::disk('s3')->url($filePath);
        }


        $role=$this->role->where('name','system_user')->first();

        $user->save();

        $user->roles()->attach($role->id);
  
        return $user;

      
    }


   
}