<?php

namespace App\UseCases\V1\Role;

use App\Models\Role;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Carbon;

class Create
{
    private $role;


    public function __construct(
        Role $role

    )
    {
        $this->role = $role;

    }

    public function execute(Request $request)
    {
  


        $role = $this->role->create([
            'name' => $request->get('name'),
            'display_name' => str_replace(' ','_',$request->get('name')),
            'description' => $request->get('description'),
        ]);



        return $role;

      
    }


   
}