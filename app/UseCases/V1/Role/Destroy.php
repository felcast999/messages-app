<?php

namespace App\UseCases\V1\Role;

use App\Models\Role;
use Illuminate\Http\Request;

class Destroy
{
    private $model;

    public function __construct(
        Role $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {
        return  $this->model->destroy($id);
    }

         public function destroy_multiple(Request $request)
    {
        return  $this->model->whereIn('id',$request->ids)->delete();
    }
}