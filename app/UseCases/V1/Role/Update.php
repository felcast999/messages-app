<?php

namespace App\UseCases\V1\Role;

use App\Models\Role;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Carbon;

class Update
{
    private $model;


    public function __construct(
        Role $model

    )
    {
        $this->model = $model;

    }

    public function execute(Request $request)
    {
        $model = $this->model->find($request->role_id);

        $request->merge(['display_name'=>str_replace(' ','_',$request->get('name'))]);
        $model->update($request->all());
       
        return $model;
    }


   
}