<?php

namespace App\UseCases\V1\Role;

use App\Models\Role;

class Show
{
    private $model;

    public function __construct(
        Role $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {
        $role = $this->model->findOrFail($id);
        
        
        return compact('role');
    }
}