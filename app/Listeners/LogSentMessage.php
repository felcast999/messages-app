<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Message;
use App\Models\StatusType;
use App\Models\Status;

use Illuminate\Support\Facades\Log;

class LogSentMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginHistory  $event
     * @return void
     */
     public function handle(MessageSending $event)
{
   
$status_type=StatusType::where('name','messages')->first();

$status=Status::where(["status_type_id"=>$status_type->id,"name"=>"sent"])->first();

$data= json_decode( json_encode($event->data), true);

$message=Message::find($data["model"]["id"]);

$message->status=$status->id;

$message->save();

Log::info("Status Cambiado");

Log::info($message);


}

public function failed(MessageSending $event, $exception)
{
Log::emergency("fail");
}

}
