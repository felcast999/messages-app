<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSending;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Message;

use Illuminate\Support\Facades\Log;

class LogSendingMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginHistory  $event
     * @return void
     */
     public function handle(MessageSending $event)
{
$data= json_decode( json_encode($event->data), true);

$message=Message::find($data["model"]["id"]);

Log::info("Estado actual");

Log::info($message);



}

public function failed(MessageSending $event, $exception)
{
#Log::emergency("fail");
}


}
