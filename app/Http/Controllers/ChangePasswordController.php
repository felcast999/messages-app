<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChangePassword\ChangePasswordRequest;
use App\Models\User;
use DB;

class ChangePasswordController extends Controller
{

       public function __construct()
    {
       $this->middleware('role:super_admin,admin,administration_manager,system_user');
    }
    public function process(ChangePasswordRequest $request)
    {

        $this->changePassword($request);
        $this->removePasswordResetRegister($request);



        return respondWithJson(true,[],'Contrasena actualizada!, Inicie sesion con nueva contrasena',200);

    
    }

    public function changePassword($request)
    {
        $user = User::whereEmail($request->email)->first();
        $user->update(['password' => $request->password]);

    }

    public function removePasswordResetRegister($request)
    {
        DB::table('password_resets')->where([
            'email' => $request->email,
            'token' => $request->resetToken
        ])->delete();

    }

}
