<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Datatables\Message\MessageDataTable;

use App\Http\Requests\V1\Message\CreateRequest;
use App\Http\Requests\V1\Message\UpdateRequest;
use App\Http\Requests\V1\Message\DestroyRequest;

use App\UseCases\V1\Message\Create;
use App\UseCases\V1\Message\Update;
use App\UseCases\V1\Message\Show;
use App\UseCases\V1\Message\Destroy;


class MessageController extends Controller
{
     
       public function __construct()
    {
       $this->middleware('role:super_admin,admin,system_user');
    }

    public function dataTable(MessageDataTable $table)
    {

         return respondWithJson(true,$table->build(),'',200);
    }


    public function store(
        CreateRequest $request,
        Create $create
    )
    {


        $result=$create->execute($request);


         return respondWithJson(true,$result,'Mensaje registrado!',200);
     
    }

 

    public function update(
        UpdateRequest $request,
        Update $update,
        $id
    )
    {

        $request->merge(["message_id"=>$id]);


        $result=$update->execute($request);


         return respondWithJson(true,$result,'Mensaje modificado!',200);


      
    }


    public function show(
        $id,
        Show $show
    )
    {
        $model = $show->execute($id);


        return respondWithJson(true,$model,'',200);

    }

   
     public function destroy(
        Destroy $destroy,
         $id
    )
    {

        $destroy->execute($id);
        
        return respondWithJson(true,[],'Mensaje eliminado!',200);

      
    }

        public function destroy_multiple(
        Destroy $destroy,
        DestroyRequest $request
    )
    {

        $destroy->destroy_multiple($request);
        
        return respondWithJson(true,[],'Mensajes eliminados!',200);

      
    }
  
}
