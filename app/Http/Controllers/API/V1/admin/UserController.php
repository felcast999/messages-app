<?php

namespace App\Http\Controllers\Api\V1\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Datatables\User\UserDataTable;

use App\Http\Requests\V1\User\CreateRequest;
use App\Http\Requests\V1\User\UpdateRequest;
use App\Http\Requests\V1\User\DestroyRequest;

use App\UseCases\V1\User\Create;
use App\UseCases\V1\User\Update;
use App\UseCases\V1\User\Show;
use App\UseCases\V1\User\Destroy;


class UserController extends Controller
{
     

      public function __construct()
    {
       $this->middleware('role:super_admin,admin');
    }

    public function dataTable(UserDataTable $table)
    {


         return respondWithJson(true,$table->build(),'',200);
    }


    public function store(
        CreateRequest $request,
        Create $create
    )
    {


        $result=$create->execute($request);


         return respondWithJson(true,$result,'Usuario registrado!',200);
     
    }

 

    public function update(
        UpdateRequest $request,
        Update $update,
        $id
    )
    {
            
        $request->merge(["user_id"=>$id]);

        $result=$update->execute($request);


         return respondWithJson(true,$result,'Usuario modificado!',200);


      
    }

        public function update_profile(
        UpdateRequest $request,
        Update $update,
    )
    {
            
        $request->merge(["user_id"=>auth()->user()->id]);

        $result=$update->execute($request);


         return respondWithJson(true,$result,'Cuenta modificada!',200);


      
    }



    public function show(
        $id,
        Show $show
    )
    {
        $model = $show->execute($id);


        return respondWithJson(true,$model,'',200);

    }

   
     public function destroy(
        Destroy $destroy,
         $id
    )
    {

        $destroy->execute($id);
        
        return respondWithJson(true,[],'Usuario eliminado!',200);

      
    }

        public function destroy_multiple(
        Destroy $destroy,
        DestroyRequest $request
    )
    {

        $destroy->destroy_multiple($request);
        
        return respondWithJson(true,[],'Usuarios eliminados!',200);

      
    }
  
}
