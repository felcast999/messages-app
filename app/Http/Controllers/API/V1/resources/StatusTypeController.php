<?php

namespace App\Http\Controllers\API\V1\resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Status;

class StatusController extends Controller
{
    protected $status;

    public function __construct(Status $status)
    {
        $this->status = $status;
    }


    public function get()
    {
        $query = $this->status->select('id','name','status_type_id')->with('statuses')->get();

        return respondWithJson(true,$query,'',200);

    }


   
}