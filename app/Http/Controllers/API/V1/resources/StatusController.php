<?php

namespace App\Http\Controllers\API\V1\resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Status;
use App\Models\StatusType;

class StatusController extends Controller
{
    protected $status;
    protected $status_type;

    public function __construct(Status $status,StatusType $status_type)
    {
        $this->status = $status;
        $this->status_type = $status_type;

    }


    public function get($type_id)
    {
        $query = $this->status->select('id','name','status_type_id')->with('status_type:id,name')->where('status_type_id',$type_id)->get();

        return respondWithJson(true,$query,'',200);

    }



    public function get_type()
    {
        $query = $this->status_type->select('id','name')->get();

        return respondWithJson(true,$query,'',200);

    }


   
}