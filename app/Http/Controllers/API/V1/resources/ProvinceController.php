<?php

namespace App\Http\Controllers\API\V1\resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Province;

class ProvinceController extends Controller
{
    protected $province;

    public function __construct(Province $province)
    {
        $this->province = $province;
    }


    public function get()
    {
        $query = $this->province->select('id','name','department_id')->get();
       
        return respondWithJson(true,$query,'',200);

        
    }


      public function get_by_department($id)
    {
        $query = $this->province->select('id','name','department_id')->where('department_id',$id)->get();

        return respondWithJson(true,$query,'',200);
 
    }

   
}