<?php

namespace App\Http\Controllers\API\V1\resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\District;

class DistrictController extends Controller
{
    protected $district;

    public function __construct(District $district)
    {
        $this->district = $district;
    }


    public function get()
    {
        $query = $this->district->select('id','name','department_id','province_id')->get();

        return respondWithJson(true,$query,'',200);

    }


      public function get_by_department($id)
    {
        $query = $this->district->select('id','name','department_id','province_id')->where('department_id',$id)->get();
       
        return respondWithJson(true,$query,'',200);

        
    }


      public function get_by_province($id)
    {
        $query = $this->district->select('id','name','department_id','province_id')->where('province_id',$id)->get();
       
        return respondWithJson(true,$query,'',200);

        
    }

   
}