<?php

namespace App\Http\Controllers\API\V1\resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Department;

class DepartmentController extends Controller
{
    protected $department;

    public function __construct(Department $department)
    {
        $this->department = $department;
    }


    public function get()
    {
        $query = $this->department->select('id','name')->get();

        return respondWithJson(true,$query,'',200);

        
    }

}