<?php

namespace App\Http\Controllers\API\V1\resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Role;

class RoleController extends Controller
{
    protected $role;

    public function __construct(Role $role)
    {
        $this->role = $role;

    }


    public function get()
    {
        $query = $this->role->select('id','display_name','description')->get();

        return respondWithJson(true,$query,'',200);

    }



   

   
}