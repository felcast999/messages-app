<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\LoginRequest;


use App\Models\User;
use App\Models\Role;
use App\Events\LoginHistory;

use DB;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {



    	$credentials = request(['email', 'password']);
    	#$credentials['is_active'] = 1;

    	try {
    		if (!auth()->attempt($credentials)) {
    			return response()->json(['error' => 'Credenciales invalidas'], 401);
    		}
    	} catch (JWTException $e) {
    		return response()->json(['error' => 'No se ha podido generar un token de sesion'], 500);
    	}

        $user=User::where('email', request(['email']))->firstOrFail();

        $user->tokens()->delete();
           
        $token=$user->createToken('auth_token')->plainTextToken;


        event(new LoginHistory($user));


        return $this->respondWithToken($token);
    }

    /**
     * registrar nuevo usuario.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
    	$user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
    		'email' => $request->get('email'),
    		'password' => $request->get('password'),
            'document_type_id'=>$request->get('document_type_id'),
            'document_number'=>$request->get('document_number'),
            'status_id'=>1
    	]);


        $admin_role = Role::where('name','admin')->first();
        $super_admin_role = Role::where('name','super_admin')->first();


        $user->roles()->attach([$admin_role->id, $super_admin_role->id]);

        $credentials = request(['email', 'password']);
       # $credentials['is_active'] = 1;

        $token=$user->createToken('auth_token')->plainTextToken;

        return $this->respondWithToken($token,$user);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $auth_user = auth()->user();
        
        $auth_user->load('roles');
       // $auth_user->withRelationships();

        return respondWithJson(true,$auth_user,'Exito!',200);

    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {


        try {


            auth()->user()->currentAccessToken()->delete();

            return response()->json(['message' => 'Session cerrada!']);

        } catch(JWTException $e)
        {
            return response()->json([
                'message' => 'Error al cerrar session, intente nuevamente',
                'error' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }

    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $user=User::find(auth()->user()->id);
        $token=$user->createToken('auth_token')->plainTextToken;

        auth()->user()->currentAccessToken()->delete();

        return respondWithJson(true,["token"=>$token],'Exito!',200);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token,$user=null)
    {

        $user_data=empty($user)?$this->me()->original:$user;

        return respondWithJson(true,[
            'access_token' => $token,
            'token_type' => 'bearer',
            //'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $user_data
        ],'Exito!',200);

    	
    }
    
}
