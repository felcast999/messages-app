<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\CheckAttributesInDeleteRecordsRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
      public function rules()
    {
        return [
            'first_name' => 'required|string|min:2|max:50',
            'last_name' => 'required|string|min:2|max:50',
            'email' => [
                "required"
            ],
            'password' => 'required|confirmed|min:6',
            'role_id' => 'required|array',
            'document_type_id'=>'required',
            'document_number'=>'required'
       
        ];
    }


    public function attributes()
    {
        return [
            'first_name' => 'Nombre',
            'last_name' => 'Apellido',
            'email' => 'Correo',
            'password' => 'Contraseña',
            'role_id' => 'Rol',
            'document_type_id.required' => 'El tipo de documento es requerido',
            'document_number.required' => 'El numero de documento es requerido'

        ];
    }

      protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(respondWithJson(false,[],$validator->errors()->get('*'),422));
    }


}
