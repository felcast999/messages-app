<?php

namespace App\Http\Requests\Auth;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CheckAttributesInDeleteRecordsRule;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required'
        ];
    }

   

    public function messages()
    {
        return [
            'email.required' => 'El email es requerido',
            'email.email' => 'El email es no es una direccion de correo vailda',
            'password.required' => 'La contrasena es requerida'
            
        ];
    }


      protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(respondWithJson(false,[],$validator->errors()->get('*'),422));
    }
}
