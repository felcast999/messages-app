<?php

namespace App\Http\Requests\V1\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\CheckAttributesInDeleteRecordsRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class DestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
         public function rules()
    {

              return [
            'ids'=>'required'

        ];

     
    }


    public function attributes()
    {
      return [
            'ids'   =>'ids'
        ];
    }




    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(respondWithJson(false,[],$validator->errors()->get('*'),422));
    }
}
