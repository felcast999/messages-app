<?php

namespace App\Http\Requests\V1\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\CheckAttributesInDeleteRecordsRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                "required",
                "string",
                "min:2",
                "max:50",
                "unique:roles,name,".$this->request->get('role_id')
            ],
            'description' => 'required|string|min:2|max:50'
        ];
    }

    public function attributes()
    {
         return [
            'name' => 'Nombre',
            'description' => 'Descripcion'

        ];
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(respondWithJson(false,[],$validator->errors()->get('*'),422));
    }
}
