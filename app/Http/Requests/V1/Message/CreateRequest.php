<?php

namespace App\Http\Requests\V1\Message;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\CheckAttributesInDeleteRecordsRule;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
       return [
            'subject'   =>[
                "required",
                "min:2"
            ],
            'receiver'    => 'required|string|min:2',
            'content'=>'required'
        
        ];
    }


    public function attributes()
    {
     return [
            'subject'     => 'Asunto',
            'receiver'   => 'Destinatario',
            'content'=>'Contenido'

        ];
    }

      protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(respondWithJson(false,[],$validator->errors()->get('*'),422));
    }

}
