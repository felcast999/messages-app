<?php

namespace App\Http\Requests\V1\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Rules\CheckAttributesInDeleteRecordsRule;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:2|max:50',
            'last_name' => 'required|string|min:2|max:50',
            'email' => [
                "required",
                "email",
                "max:50",
                new CheckAttributesInDeleteRecordsRule('users'),
                "unique:users",
            ],
            'password' => 'required|confirmed|min:6',
            'birthdate'=>'required',
            'document_number'=>'required',
            'department_id'=>'required'
            
       
        ];
    }


    public function attributes()
    {
        return [
            'first_name' => 'Nombre',
            'last_name' => 'Apellido',
            'email' => 'Correo',
            'password' => 'Contrasena',
            'birthdate' => 'Fecha de nacimiento',
            'document_number' => 'numero de documento',
            'department_id'=>'Ciudad'

        ];
    }

      protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(respondWithJson(false,[],$validator->errors()->get('*'),422));
    }

}
