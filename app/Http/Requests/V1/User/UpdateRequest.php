<?php

namespace App\Http\Requests\V1\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\CheckAttributesInDeleteRecordsRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      $id=$this->route('user');

      if (empty($id)) 
      {
        $id=auth()->user()->id;
      }
      
          return [
            'first_name' => 'required|string|min:2|max:50',
            'last_name' => 'required|string|min:2|max:50',
            /*'email' => [
                "required",
                "email",
                "max:50",
                new CheckAttributesInDeleteRecordsRule('users'),
                "unique:users,email,".$id,
            ],*/
            'password' => 'required_if:change_password,true|confirmed|min:6',
            'birthdate'=>'required',
            #'document_number'=>'required'
       
        ];
    }

    public function attributes()
    {
         return [
            'first_name' => 'Nombre',
            'last_name' => 'Apellido',
            #'email' => 'Correo',
            'password' => 'Contrase\ña',
            'birthdate.required' => 'Fecha de nacimiento',
            #'document_number.required' => 'Numero documento'

        ];
    }


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(respondWithJson(false,[],$validator->errors()->get('*'),422));
    }
}
