<?php

namespace App\Services;
use App\Models\ExchangeRate;

class GlobalService {

    private $GLOBAL_CURRENT_EXCHANGE_RATE;

  
    public function get()
    {

        $GLOBAL_CURRENT_EXCHANGE_RATE=ExchangeRate::select('id','pen','exchange_date')->latest()->first();

        return $GLOBAL_CURRENT_EXCHANGE_RATE;
    }
}