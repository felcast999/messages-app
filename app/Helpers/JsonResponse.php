<?php

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

if (! function_exists('respondWithJson')) {
    function respondWithJson($success,$data=[], $message='',$status_code=200)
	{
		return response()->json(
            [
                'success' => $success,
                'data' => $data,
                'message' => $message,
                'status'=>$status_code
            ],  $status_code
        );
	}

}

