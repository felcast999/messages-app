<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use HasFactory,SoftDeletes;


    protected $table="messages";

    protected $fillable=["subject","receiver","content","status_id","user_id"];


     public function status()
    {

    return $this->belongsTo(Status::class,'status_id');

    }


     public function user()
    {

    return $this->belongsTo(User::class,'user_id');

    }
}
