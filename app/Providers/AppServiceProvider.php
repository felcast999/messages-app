<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\GlobalService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->singleton('GLOBAL_CURRENT_EXCHANGE_RATE', function() {

             $global = new GlobalService();

             $exchange_rate=$global->get();

              return $exchange_rate;
            });
    }
}
