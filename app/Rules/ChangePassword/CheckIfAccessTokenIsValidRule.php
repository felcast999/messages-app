<?php

namespace App\Rules\ChangePassword;

use Illuminate\Contracts\Validation\Rule;
use DB;

class CheckIfAccessTokenIsValidRule implements Rule
{
    protected $email;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $token)
    {
        $tokenIsValid =  DB::table('password_resets')
                            ->where([
                                    'email' => $this->email,
                                    'token' => $token 
                                ])->first();

        if ($tokenIsValid)
            return true;

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El email o el token proporcionado no es valido';
    }
}
