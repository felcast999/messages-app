<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use DB;

class CheckAttributesInDeleteRecordsRule implements Rule
{
    protected $table;
    protected $connection;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($table, $connection = null)
    {
        $this->table = $table;
        $this->connection = $connection;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // añadir conexion
        if (!$this->connection)
            $this->connection = 'mysql';

        $check = DB::connection($this->connection)->table($this->table)->where('id',$value)->whereNotNull('deleted_at')->first();

        // si se usa soft deletes hay comportamiento extraño al usar restricciones unique en la bd
        if($recordDeletedWithThisAttribute)
        {   
            $date = \Carbon\Carbon::now()->format('d-m-Y H:i:m');

            // modificar el registro si hay registros eliminados con el atributo a asignar
            $attributeInUse = $recordDeletedWithThisAttribute->$attribute;
            DB::connection($this->connection)->table($this->table)->where('id',$recordDeletedWithThisAttribute->id)->update([$attribute => "{$date}__{$attributeInUse}"]);
        }

        return true;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Error desconocido';
    }
}
