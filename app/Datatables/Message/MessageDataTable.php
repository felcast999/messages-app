<?php

namespace App\Datatables\Message;
use App\Http\Controllers\Controller;

use App\Datatables\BaseDataTable;
use App\Models\Message;
use Yajra\DataTables\EloquentDataTable;

class MessageDataTable extends Controller
{
	
	
		public function build()
	{	


	$status_value=request('status_value');
	$search_value=request('search_value');

	$query= new Message;
	$query= $query->select('messages.*','statuses.name as status_name','users.first_name','users.last_name')
    ->join('statuses','messages.status_id','=','statuses.id')
    ->join('users','messages.user_id','=','users.id');

    $sql='CONCAT(messages.subject," ",messages.receiver," ",messages.content," ",statuses.name," ",users.first_name," ",users.last_name) like ?';

					





								
								
								if (request()->has('search_value') && !empty(request('search_value'))) 
								{



								$query=$query->whereRaw($sql, ["%{$search_value}%"]);




								    
								}


								if (!empty($status_value)) 
								{

									 $query=$query->whereIn('status_id',$status_value);
									
								}

								


							$filter=strtolower(request('filter'));							

							switch ($filter) {

								case 'asc':
								$query->orderBy(request('order_column'),'ASC');
								break;
								
								case 'desc':
								
								$query->orderBy(request('order_column'),'DESC');

								break;
							}

								

					
							
						
						
					


    $query=$query->paginate(request('length'));
    return $query;
		
	}


	

}