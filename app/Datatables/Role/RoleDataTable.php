<?php

namespace App\Datatables\Role;

use App\Datatables\BaseDataTable;
use App\Models\Role;
use Yajra\DataTables\EloquentDataTable;

class RoleDataTable extends BaseDataTable
{
	public function __construct(Role $model)
	{
		parent::__construct($model);
	}

	protected function makeQuery():void
	{	
		$query = $this->model
						->query()
                        ->select('roles.*'); 

		$this->setQuery($query);
	}

	protected function makeColumns():EloquentDataTable
	{
		return $this->new_dataTable;
	}

}