<?php

namespace App\Datatables\User;
use App\Http\Controllers\Controller;

use App\Datatables\BaseDataTable;
use App\Models\User;
use Yajra\DataTables\EloquentDataTable;

class UserDataTable extends Controller
{
	
	
		public function build()
	{	


	$role_value=request('role_value');
	$status_value=request('status_value');
	$document_value=request('document_value');
	$search_value=request('search_value');

	$query= new User;
	$query= $query->select('users.*')
    ->join('statuses','users.status_id','=','statuses.id')
    ->with('roles:id,name,display_name,description')->with('status:id,name'); 

  $sql='CONCAT(users.first_name," ",users.last_name," ",users.birthdate," ",users.document_number," ",users.email," ",users.phone," ",statuses.name) like ?';

					





								
								
								if (request()->has('search_value') && !empty(request('search_value'))) 
								{



								$query=$query->whereRaw($sql, ["%{$search_value}%"])->orWhereHas('roles',function($q) use ($search_value)
								{

								  $q->where('name','like','%'.$search_value.'%');
								});	




								    
								}


									

									if (!empty($status_value)) 
									{

									 $query=$query->whereIn('status_id',$status_value);
									
									}

									if (!empty($role_value)) 
									{

									 $query=$query->whereHas('roles',function($q) use ($role_value)
								{

								  $q->whereIn('id',$role_value);
								});	
									
									}

							$filter=strtolower(request('filter'));							

							switch ($filter) {

								case 'asc':
								$query->orderBy(request('order_column'),'ASC');
								break;
								
								case 'desc':
								
								$query->orderBy(request('order_column'),'DESC');

								break;
							}

								

					
							
						
						
					


    $query=$query->paginate(request('length'));
    return $query;
		
	}


	

}