<h3>Prueba tecnica </h3>

<div style="text-align: center">

<ul>
<li>Clonar Repositorio</li>
<li>Ir a la raiz del proyecto y ejecutar el comando composer install</li>
<li>Crear la base de datos y añadir archivo .env</li>
<li>ir a la carpeta resources/core_ui y ejecutar el comando npm install</li>
<li>Ejecutar migraciones y seed (php artisan migrate) (php artisan db:seed)</li>
<li>En carpeta resources/core_ui y ejecutar el comando ng build --prod</li>
<li>Ejecutar el comando php artisan storage:link</li>
<li>Ejecutar el comando php artisan serve</li>
<li>Al autenticar, añadir el bearer token a las carpetas que lo requieran</li>
<li>Para revisar el status del envio del correo, revisar los logs dentro de storage/logs/laravel.log</li>
</ul>

</div>